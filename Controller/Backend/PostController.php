<?php

namespace PPB\BlogBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PPB\BlogBundle\Entity\Post;
use PPB\BlogBundle\Form\PostType;

/**
 * Post controller.
 *
 * @Route("/post/{post_type}")
 */
class PostController extends Controller
{
    /**
     * Lists all Post entities.
     *
     * @Route("/", name="backend_post_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($post_type)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PPBBlogBundle:Post')->findByType($post_type);

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/", name="backend_post_create")
     * @Method("POST")
     * @Template("PPBBlogBundle:Backend\Post:edit.html.twig")
     */
    public function createAction(Request $request, $post_type)
    {
        $entity = new Post();
        $entity->setAuthor($this->get('security.context')->getToken()->getUser());
        $entity->setType($post_type);
        $form = $this->createForm(new PostType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_post_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Post entity.
     *
     * @Route("/new", name="backend_post_new")
     * @Method("GET")
     * @Template("PPBBlogBundle:Backend\Post:edit.html.twig")
     */
    public function newAction()
    {
        $entity = new Post();
        $form   = $this->createForm(new PostType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", name="backend_post_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($post_type, Post $entity)
    {
        if($post_type != $entity->getType()) {
            throw $this->createNotFoundException('The Post does not exist');
        }
        $form = $this->createForm(new PostType(), $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Post entity.
     *
     * @Route("/{id}", name="backend_post_update")
     * @Method("PUT")
     * @Template("PPBBlogBundle:Backend\Post:edit.html.twig")
     */
    public function updateAction(Request $request, $post_type, Post $entity)
    {
        if($post_type != $entity->getType()) {
            throw $this->createNotFoundException('The Post does not exist');
        }
        $em = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createForm(new PostType(), $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

           // return $this->redirect($this->generateUrl('backend_post_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}", name="backend_post_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Post $entity)
    {
        $form = $this->createDeleteForm($entity->getId());
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_post_index'));
    }

    /**
     * Creates a form to delete a Post entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
