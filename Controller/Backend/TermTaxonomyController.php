<?php

namespace PPB\BlogBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use PPB\BlogBundle\Entity\TermTaxonomy;
use PPB\BlogBundle\Form\TermTaxonomyType;

/**
 * TermTaxonomy controller.
 *
 * @Route("/taxonomy/{taxonomy}")
 */
class TermTaxonomyController extends Controller
{
    /**
     * Lists all TermTaxonomy entities.
     *
     * @Route("/", name="backend_taxonomy_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($taxonomy)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PPBBlogBundle:TermTaxonomy')->findByTaxonomy($taxonomy);

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new TermTaxonomy entity.
     *
     * @Route("/", name="backend_taxonomy_create")
     * @Method("POST")
     * @Template("PPBBlogBundle:TermTaxonomy:new.html.twig")
     */
    public function createAction(Request $request, $taxonomy)
    {
        $entity  = new TermTaxonomy();
        $entity->setTaxonomy($taxonomy);
        $form = $this->createForm(new TermTaxonomyType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_taxonomy_edit', array('id' => $entity->getId(), 'taxonomy' => $taxonomy)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new TermTaxonomy entity.
     *
     * @Route("/new", name="backend_taxonomy_new")
     * @Method("GET")
     * @Template("PPBBlogBundle:Backend/TermTaxonomy:edit.html.twig")
     */
    public function newAction()
    {
        $entity = new TermTaxonomy();
        $form   = $this->createForm(new TermTaxonomyType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TermTaxonomy entity.
     *
     * @Route("/{id}/edit", name="backend_taxonomy_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(TermTaxonomy $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new TermTaxonomyType(), $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing TermTaxonomy entity.
     *
     * @Route("/{id}", name="backend_taxonomy_update")
     * @Method("PUT")
     * @Template("PPBBlogBundle:TermTaxonomy:edit.html.twig")
     */
    public function updateAction(Request $request, TermTaxonomy $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($entity->getId());
        $form = $this->createForm(new TermTaxonomyType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_taxonomy_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a TermTaxonomy entity.
     *
     * @Route("/{id}", name="backend_taxonomy_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TermTaxonomy $entity)
    {
        $form = $this->createDeleteForm($entity->getId());
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_taxonomy_index'));
    }

    /**
     * Creates a form to delete a TermTaxonomy entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
