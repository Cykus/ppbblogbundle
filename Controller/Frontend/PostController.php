<?php

namespace PPB\BlogBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Post controller.
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="post_index")
     * @Template()
     */
    public function indexAction()
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');

        $entities = $entityManager
            ->getRepository('PPBBlogBundle:Post')
            ->findPublishedPosts(10);


        return array('entities' => $entities);
    }

    /**
     * @Route("/category/{slug}", name="post_category")
     * @Template("PPBBlogBundle:Frontend\Post:index.html.twig")
     */
    public function categoryAction($slug)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');

        $entities = $entityManager
            ->getRepository('PPBBlogBundle:Post')
            ->getPublishedPostsByCategoryQuery($slug)->getResult();


        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Post entity.
     *
     * @Route("/{slug}", name="post_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PPBBlogBundle:Post')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }

        return array(
            'entity'      => $entity,
        );
    }

}
