<?php

namespace PPB\BlogBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Post controller.
 */
class WidgetController extends Controller
{

    /**
     * @Route("/widget/categories", name="widget_categories")
     * @Template()
     */
    public function categoriesAction()
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');

        $entities = $entityManager
            ->getRepository('PPBBlogBundle:Term')
            ->findAllCategories();


        return array('entities' => $entities);
    }

}
