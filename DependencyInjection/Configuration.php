<?php

namespace PPB\BlogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ppb_blog');

        $this->addPostTypesSection($rootNode);
        $this->addTaxonomiesSection($rootNode);

        return $treeBuilder;
    }

    private function addPostTypesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('post_types')
                ->prototype('array')
                    ->children()
                        ->scalarNode('name')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('slug')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->booleanNode('hierarchical')->end()
                        ->arrayNode('supports')
                            ->prototype('scalar')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    private function addTaxonomiesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('taxonomies')
                ->prototype('array')
                    ->children()
                        ->scalarNode('name')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('slug')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->booleanNode('hierarchical')->end()
                        ->arrayNode('object_type')
                            ->prototype('scalar')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
