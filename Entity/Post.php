<?php

namespace PPB\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="ppb_posts")
 * @ORM\Entity(repositoryClass="PPB\BlogBundle\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{
    const STATUS_PUBLISH    = 'publish';
    const STATUS_INHERIT    = 'inherit';
    const STATUS_DRAFT      = 'draft';
    const STATUS_AUTO_DRAFT = 'auto-draft';

    const TYPE_ATTACHMENT   = 'attachment';
    const TYPE_PAGE         = 'page';
    const TYPE_POST         = 'post';
    const TYPE_REVISION     = 'revision';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User $author
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="post_author", referencedColumnName="ID", onDelete="CASCADE")
     */
    private $author;

    /**
     * @var DateTime $publishedAt
     *
     * @ORM\Column(name="post_date", type="datetime", nullable=false)
     */
    private $publishedAt;

    /**
     * @var DateTime $publishedAtAsGmt
     *
     * @ORM\Column(name="post_date_gmt", type="datetime", nullable=false)
     */
    private $publishedAtAsGmt;

    /**
     * @var text $content
     *
     * @ORM\Column(name="post_content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var text $title
     *
     * @ORM\Column(name="post_title", type="text", nullable=false)
     */
    private $title;

    /**
     * @var text $excerpt
     *
     * @ORM\Column(name="post_excerpt", type="text", nullable=false)
     */
    private $excerpt;

    /**
     * @var string $postStatus
     *
     * @ORM\Column(name="post_status", type="string", length=20, nullable=false)
     */
    private $status;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"title"}, updatable=true, separator="_")
     * @ORM\Column(name="post_name", type="string", length=200, nullable=false, unique=true)
     */
    private $slug;

    /**
     * @var DateTime $modifiedAt
     *
     * @ORM\Column(name="post_modified", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var DateTime $modifiedAtAsGmt
     *
     * @ORM\Column(name="post_modified_gmt", type="datetime", nullable=false)
     */
    private $modifiedAtAsGmt;

    /**
     * @var integer $parentId
     *
     * @ORM\Column(name="post_parent", type="bigint", nullable=false)
     * @ORM\JoinColumn(name="post_parent", referencedColumnName="ID")
     */
    private $parentId;

    /**
     * @var integer $menuOrder
     *
     * @ORM\Column(name="menu_order", type="integer", nullable=false)
     */
    private $menuOrder;

    /**
     * @var string $type
     *
     * @ORM\Column(name="post_type", type="string", length=20, nullable=false)
     */
    private $type;

    /**
     * @var Collection $termRelationships
     *
     * @ORM\ManyToMany(targetEntity="TermTaxonomy", inversedBy="posts")
     * @ORM\JoinTable(name="ppb_term_relationship",
     *      joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="ID")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="termtaxonomy_id", referencedColumnName="term_taxonomy_id")})
     */
    private $termTaxonomies;

    public function __construct()
    {
        $this->parentId = 0;
        $this->menuOrder = 0;
        $this->type = self::TYPE_POST;
        $this->status = self::STATUS_DRAFT;
        $this->publishedAt = new \DateTime();
        $this->publishedAtAsGmt = new \DateTime(null, new \DateTimeZone('UTC'));
        $this->modifiedAt = new \DateTime();
        $this->modifiedAtAsGmt = new \DateTime(null, new \DateTimeZone('UTC'));
        $this->termTaxonomies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;
        $this->excerpt = $content;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Post
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set publishedAtAsGmt
     *
     * @param \DateTime $publishedAtAsGmt
     * @return Post
     */
    public function setPublishedAtAsGmt($publishedAtAsGmt)
    {
        $this->publishedAtAsGmt = $publishedAtAsGmt;
    
        return $this;
    }

    /**
     * Get publishedAtAsGmt
     *
     * @return \DateTime 
     */
    public function getPublishedAtAsGmt()
    {
        return $this->publishedAtAsGmt;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set excerpt
     *
     * @param string $excerpt
     * @return Post
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;
    
        return $this;
    }

    /**
     * Get excerpt
     *
     * @return string 
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Post
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public static function getStatusList()
    {
        return array(
            self::STATUS_DRAFT => 'draft',
            self::STATUS_PUBLISH => 'publish',
        );
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @ORM\PreUpdate
     * Set modifiedAt
     *
     * @return Post
     */
    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime();
    
        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @ORM\PreUpdate
     * Set modifiedAtAsGmt
     *
     * @return Post
     */
    public function setModifiedAtAsGmt()
    {
        $this->modifiedAtAsGmt = new \DateTime(null, new \DateTimeZone('UTC'));
    
        return $this;
    }

    /**
     * Get modifiedAtAsGmt
     *
     * @return \DateTime 
     */
    public function getModifiedAtAsGmt()
    {
        return $this->modifiedAtAsGmt;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Post
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    
        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     * @return Post
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;
    
        return $this;
    }

    /**
     * Get menuOrder
     *
     * @return integer 
     */
    public function getMenuOrder()
    {
        return $this->menuOrder;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Post
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set author
     *
     * @param \PPB\BlogBundle\Entity\User $author
     * @return Post
     */
    public function setAuthor(\PPB\BlogBundle\Entity\User $author = null)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return \PPB\BlogBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add termTaxonomies
     *
     * @param \PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies
     * @return Post
     */
    public function addTermTaxonomy(\PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies)
    {
        $this->termTaxonomies[] = $termTaxonomies;

        return $this;
    }

    /**
     * Remove termTaxonomies
     *
     * @param \PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies
     */
    public function removeTermTaxonomy(\PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies)
    {
        $this->termTaxonomies->removeElement($termTaxonomies);
    }

    /**
     * Get termTaxonomies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermTaxonomies()
    {
        return $this->termTaxonomies;
    }

    /**
     * Get termTaxonomies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTermsByTaxonomy($taxonomy)
    {
        if (!is_null($this->termTaxonomies)) {
            $result = new \Doctrine\Common\Collections\ArrayCollection();
            foreach ($this->termTaxonomies as $termTaxonomy) {
                if ($taxonomy  == $termTaxonomy->getTaxonomy()) {
                    $result[] = $termTaxonomy;
                }
            }
            return $result;
        }

        return null;
    }
}
