<?php

namespace PPB\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="ppb_terms")
 * @ORM\Entity(repositoryClass="PPB\BlogBundle\Repository\TermRepository")
 */
class Term
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="term_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="_")
     * @ORM\Column(name="slug", type="string", length=200, nullable=false)
     */
    private $slug;

    /**
     * @var integer $group
     *
     * @ORM\Column(name="term_group", type="bigint", nullable=false)
     */
    private $group;

    /**
     * @var Collection $termTaxonomies
     *
     * @ORM\OneToMany(targetEntity="TermTaxonomy", mappedBy="term")
     * @ORM\JoinColumn(name="term_id", referencedColumnName="term_id")
     */
    private $termTaxonomies;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->group = 0;
        $this->termTaxonomies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return integer
     */
    public function getFrequency()
    {
        return $this->getPostCount();
    }

    /**
     * @return integer
     */
    public function getPostCount()
    {
        $termTaxonomy = $this->getPostTagTaxonomy();

        return is_null($termTaxonomy) ? 0 : $termTaxonomy->getPostCount();
    }

    /**
     * @return PSS\Bundle\BlogBundle\Entity\TermTaxonomy
     */
    private function getPostTagTaxonomy()
    {
        if (!is_null($this->termTaxonomies)) {
            foreach ($this->termTaxonomies as $termTaxonomy) {
                if (TermTaxonomy::POST_TAG  == $termTaxonomy->getTaxonomy()) {
                    return $termTaxonomy;
                }
            }
        }

        return null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Term
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Term
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set group
     *
     * @param integer $group
     * @return Term
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return integer 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Add termTaxonomies
     *
     * @param \PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies
     * @return Term
     */
    public function addTermTaxonomy(\PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies)
    {
        $this->termTaxonomies[] = $termTaxonomies;

        return $this;
    }

    /**
     * Remove termTaxonomies
     *
     * @param \PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies
     */
    public function removeTermTaxonomy(\PPB\BlogBundle\Entity\TermTaxonomy $termTaxonomies)
    {
        $this->termTaxonomies->removeElement($termTaxonomies);
    }

    /**
     * Get termTaxonomies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTermTaxonomies()
    {
        return $this->termTaxonomies;
    }
}
