<?php

namespace PPB\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ppb_term_taxonomy")
 * @ORM\Entity
 */
class TermTaxonomy
{
    const POST_TAG      = 'post_tag';
    const CATEGORY      = 'category';
    const LINK_CATEGORY = 'link_category';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="term_taxonomy_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Term $term
     *
     * @ORM\ManyToOne(targetEntity="Term", inversedBy="termTaxonomies", cascade={"persist"})
     * @ORM\JoinColumn(name="term_id", referencedColumnName="term_id")
     */
    private $term;

    /**
     * @var string $taxonomy
     *
     * @ORM\Column(name="taxonomy", type="string", length=32, nullable=false)
     */
    private $taxonomy;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer $parentId
     *
     * @ORM\ManyToOne(targetEntity="TermTaxonomy")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="term_taxonomy_id")
     */
    private $parent;

    /**
     * @var integer $count
     *
     * @ORM\Column(name="count", type="bigint", nullable=false)
     */
    private $count;

    /**
     * @var Collection $termRelationships
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="termTaxonomies")
     */
    private $posts;

    /**
     * @return string
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * @return integer
     */
    public function getPostCount()
    {
        return $this->count;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->count = 0;
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taxonomy
     *
     * @param string $taxonomy
     * @return TermTaxonomy
     */
    public function setTaxonomy($taxonomy)
    {
        $this->taxonomy = $taxonomy;

        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TermTaxonomy
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return TermTaxonomy
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set term
     *
     * @param \PPB\BlogBundle\Entity\Term $term
     * @return TermTaxonomy
     */
    public function setTerm(\PPB\BlogBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \PPB\BlogBundle\Entity\Term 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Add posts
     *
     * @param \PPB\BlogBundle\Entity\Post $posts
     * @return TermTaxonomy
     */
    public function addPost(\PPB\BlogBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;

        return $this;
    }

    /**
     * Remove posts
     *
     * @param \PPB\BlogBundle\Entity\Post $posts
     */
    public function removePost(\PPB\BlogBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set parent
     *
     * @param \PPB\BlogBundle\Entity\TermTaxonomy $parent
     * @return TermTaxonomy
     */
    public function setParent(\PPB\BlogBundle\Entity\TermTaxonomy $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \PPB\BlogBundle\Entity\TermTaxonomy 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
