<?php
namespace PPB\BlogBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(name="ID", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Collection $posts
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     * @ORM\JoinColumn(name="ID", referencedColumnName="post_author")
     */
    private $posts;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add posts
     *
     * @param \PPB\BlogBundle\Entity\Post $posts
     * @return User
     */
    public function addPost(\PPB\BlogBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;
    
        return $this;
    }

    /**
     * Remove posts
     *
     * @param \PPB\BlogBundle\Entity\Post $posts
     */
    public function removePost(\PPB\BlogBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }
    
}
