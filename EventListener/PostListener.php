<?php
namespace PPB\BlogBundle\EventListener;

use PPB\BlogBundle\Controller\Backend\PostController;
use PPB\BlogBundle\Utility\Utility;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class PostListener {

    private $post_types;

    public function __construct($post_types)
    {
        $this->post_types = $post_types;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof PostController) {
            $route_params = $event->getRequest()->attributes->get('_route_params');
            if (!Utility::in_array_r($route_params['post_type'], $this->post_types, 'slug')) {

                throw new AccessDeniedHttpException('This action needs a valid post type!');
            }
        }
    }
}