<?php

namespace PPB\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use PPB\BlogBundle\Entity\Post;
use PPB\BlogBundle\Entity\TermTaxonomy;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => false,
            ))
            ->add('content', null, array(
                'label' => false,
            ))
            ->add('status', 'choice', array(
                'label' => false,
                'choices' => Post::getStatusList()
            ))
            ->add('publishedAt', 'datetime', array(
                'label' => false,
                'date_widget'=> 'single_text',
                'time_widget'=> 'single_text',
            ))
            ->add('termTaxonomies', 'entity', array(
                    'class' => 'PPBBlogBundle:TermTaxonomy',
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                        return $er->createQueryBuilder('tt')
                            ->join('tt.term', 't')
                            ->where('tt.taxonomy = \''.TermTaxonomy::CATEGORY.'\'');
                    },
                    'property' => 'term.name',
                    'expanded' => true,
                    'multiple' => true,
                    'label' => false,
            ));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PPB\BlogBundle\Entity\Post'
        ));
    }

    public function getName()
    {
        return 'ppb_blogbundle_posttype';
    }
}
