<?php

namespace PPB\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TermTaxonomyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('term', new TermType(), array(
                'label' => false,
            ))
            ->add('parent', null, array(
                'property' => 'term.name',
            ))
            ->add('description')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PPB\BlogBundle\Entity\TermTaxonomy'
        ));
    }

    public function getName()
    {
        return 'ppb_blogbundle_termtaxonomytype';
    }
}
