<?php
namespace PPB\BlogBundle\Menu;


use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Mopa\Bundle\BootstrapBundle\Navbar\AbstractNavbarMenuBuilder;

class Builder extends AbstractNavbarMenuBuilder
{
    protected $securityContext;
    protected $isLoggedIn;

    public function __construct(FactoryInterface $factory, SecurityContextInterface $securityContext, $post_types, $taxonomies)
    {
        parent::__construct($factory);

        $this->securityContext = $securityContext;
        $this->isLoggedIn = $this->securityContext->isGranted('IS_AUTHENTICATED_FULLY');
        $this->post_types = $post_types;
        $this->taxonomies = $taxonomies;
    }

    public function createBackendSidebarMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav main-menu');

        $menu->addChild('Main')->setAttribute('class', 'nav-header');
        $menu->addChild('Dashboard', array('route' => 'fos_user_security_login'));
        $this->addDivider($menu, true);
        foreach($this->post_types as $key => $post_type) {
            $$key = $menu->addChild($post_type['name'], array('route' => 'backend_post_index', 'routeParameters' => array('post_type' => $key)));
            $$key->setChildrenAttribute('class', 'nav');
            $$key->addChild('All '.$post_type['name'], array('route' => 'backend_post_index', 'routeParameters' => array('post_type' => $key)));
            $$key->addChild('New '.$post_type['name'], array('route' => 'backend_post_new', 'routeParameters' => array('post_type' => $key)));
            foreach($this->taxonomies as $taxonomy) {
                if(in_array($key, $taxonomy['object_type']))
                    $$key->addChild($taxonomy['name'], array('route' => 'backend_taxonomy_index', 'routeParameters' => array('taxonomy' => $taxonomy['slug'])));
            }
        }


        return $menu;
    }
}