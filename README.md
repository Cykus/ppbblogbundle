# PPBBlogBundle

Symfony2 blog bundle based on the WordPress.

Some Features include:

* URLs SEO friendly
* Custom Taxonomies
* Blog archive

## Bundle Installation

### Get the bundle

Add to your `/composer.json` file :

``` json
    "require": {
        ...
        "ppb/blog-bundle": "dev-master"
    },
```

And make

``` bash
composer update
```

### Register the bundle

``` php
<?php

  // app/AppKernel.php
  public function registerBundles()
  {
    return array(
      // ...
      new PPB\BlogBundle\PPBBlogBundle(),
      );
  }
```

### Add routes

In your routing.yml you need to publish Backend and Frontend in your prefered paths

``` yml
ppb_blog_frontend:
    resource: "@PPBBlogBundle/Controller/Frontend/"
    type:     annotation
    prefix:   /

ppb_blog_backend:
    resource: "@PPBBlogBundle/Controller/Backend/"
    type:     annotation
    prefix:   /admin
```

Setting variables

You need to add something like this in your config.yml

``` yml
ppb_blog:
    post_types:
        post:
            name: Post
            slug: post
            supports: [title, content]
        page:
            name: Page
            slug: page
            supports: [title, content, parent]
    taxonomies:
        category:
            name: Category
            slug: category
            object_type: [post]
        tag:
            name: Tag
            slug: tag
            object_type: [post, page]
```

## Contact

You can contact with me on [twitter](https://twitter.com/cykus).