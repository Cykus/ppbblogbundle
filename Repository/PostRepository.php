<?php

namespace PPB\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PPB\BlogBundle\Entity\Post;
use PPB\BlogBundle\Entity\TermTaxonomy;

class PostRepository extends EntityRepository
{
    /**
     * @return Doctrine\ORM\Query
     */
    public function getPublishedPostsQuery()
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT p, a FROM \PPB\BlogBundle\Entity\Post p
             INNER JOIN p.author a
             WHERE p.type = :type AND p.status = :status
             ORDER BY p.publishedAt DESC'
        );

        $query->setParameter('type', Post::TYPE_POST);
        $query->setParameter('status', Post::STATUS_PUBLISH);

        return $query;
    }

    /**
     * @param integer $max
     * @return PPB\BlogBundle\Entity\Post
     */
    public function findPublishedPosts($max = 10)
    {
        $query = $this->getPublishedPostsQuery();
        $query->setMaxResults($max);

        return $query->getResult();
    }

    /**
     * @return Doctrine\ORM\Query
     */
    public function getPublishedPostsByTaxonomyQuery($taxonomy, $termSlug)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT p, a FROM \PPB\BlogBundle\Entity\Post p
             INNER JOIN p.author a
             INNER JOIN p.termTaxonomies tt
             INNER JOIN tt.term t
             WHERE p.type = :type
               AND p.status = :status
               AND tt.taxonomy = :taxonomy
               AND t.slug = :slug
             ORDER BY p.publishedAt DESC'
        );

        $query->setParameter('type', Post::TYPE_POST);
        $query->setParameter('status', Post::STATUS_PUBLISH);
        $query->setParameter('slug', $termSlug);
        $query->setParameter('taxonomy', $taxonomy);

        return $query;
    }

    /**
     * @return Doctrine\ORM\Query
     */
    public function getPublishedPostsByCategoryQuery($termSlug)
    {
        return $this->getPublishedPostsByTaxonomyQuery(TermTaxonomy::CATEGORY, $termSlug);
    }

}