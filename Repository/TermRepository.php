<?php

namespace PPB\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PPB\BlogBundle\Entity\TermTaxonomy;

class TermRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getTermsByTaxonomyQuery($taxonomy)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT t, tt FROM \PPB\BlogBundle\Entity\Term t
             INNER JOIN t.termTaxonomies tt
             WHERE tt.taxonomy = :taxonomy
             ORDER by t.name ASC'
        );

        $query->setParameter('taxonomy', $taxonomy);

        return $query;
    }
    /**
     * @return array
     */
    public function findAllTags()
    {
        $query = $this->getTermsByTaxonomyQuery(TermTaxonomy::POST_TAG);

        return $query->getResult();
    }

    /**
     * @return array
     */
    public function findAllCategories()
    {
        $query = $this->getTermsByTaxonomyQuery(TermTaxonomy::CATEGORY);

        return $query->getResult();
    }
}