<?php

namespace PPB\BlogBundle\Utility;


class Utility {

    public static function in_array_r($needle, $haystack, $field) {
        foreach ($haystack as $item) {
           if (is_array($item) && isset($item[$field]) && $item[$field] === $needle) {
                return true;
            }
        }
        return false;
    }
}